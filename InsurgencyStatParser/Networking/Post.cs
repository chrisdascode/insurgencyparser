﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace InsurgencyStatParser.Networking
{
    public class Post
    {
        public static async void Send(WebRequest request, string packetJson)
        {
            try
            {
                request.Method = "POST";
                request.ContentType = "application/json";
                using (var stream = new StreamWriter(await request.GetRequestStreamAsync()))
                {
                    stream.AutoFlush = true;
                    await stream.WriteAsync(packetJson);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static async Task<string> GetResponce(WebRequest request)
        {
            var response = (HttpWebResponse) await request.GetResponseAsync();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                return result;
            }
        }
    }
}