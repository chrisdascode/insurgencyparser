﻿using System;
using System.Collections.Generic;
using System.IO;
using CoreFtp;
using CoreFtp.Infrastructure;
using InsurgencyStatParser.Ftp;

namespace InsurgencyStatParser.Networking.Ftp
{
    public class ConnectNGrab : IDisposable
    {
        public delegate void EventHandler(object arg);

        private readonly List<Log> _logList = new List<Log>();
        private readonly FtpClientConfiguration _config;
        private readonly Config _setting;

        public ConnectNGrab(FtpClientConfiguration config, Config c)
        {
            _config = config;
            _setting = c;
        }


        public void Dispose()
        {
        }

        public async void Run()
        {
            try
            {
                using (var ftpClient = new FtpClient(_config))
                {
                    await ftpClient.LoginAsync();
                    if (ftpClient.IsAuthenticated)
                    {
                        Connected.Invoke(null);
                        var ftpServersLogList = await ftpClient.ListAllAsync();
                        foreach (var log in ftpServersLogList)
                        {
                            if (log.Size == 0) continue;
                            string data;
                            var dstream = await ftpClient.OpenFileReadStreamAsync(log.Name);
                            using (var streamReader = new StreamReader(dstream))
                            {
                                data = await streamReader.ReadToEndAsync();
                            }

                            var _log = new Log
                            {
                                Name = log.Name,
                                Size = log.Size,
                                Data = data
                            };
                            //Copy the file to memory
                            _logList.Add(_log);

                            if (_setting.MySetting.RemoveFilesOnEnd)
                            {
                                await ftpClient.DeleteFileAsync(log.Name);
                            }

                        }
                    }

                    await ftpClient.LogOutAsync();
                    if (!ftpClient.IsAuthenticated)
                        Disconnected?.Invoke(_logList);
                }
            }
            catch (FtpException ftpException)
            {
                if (!string.IsNullOrWhiteSpace(ftpException.Message)) FTPError.Invoke(ftpException.Message);
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrWhiteSpace(ex.Message)) FTPError.Invoke(ex.Message);
            }
        }

        #region "Events" 

        public event EventHandler Connected;
        public event EventHandler Disconnected;
        public event EventHandler FTPError;

        #endregion
    }
}