﻿namespace InsurgencyStatParser.Ftp
{
    public class Log
    {
        #region "Structure of a Log"

        public string Name { get; set; }
        public long Size { get; set; }
        public string Data { get; set; }

        #endregion
    }
}