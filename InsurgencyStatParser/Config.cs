﻿#region "Imports"

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using CoreFtp;
using CoreFtp.Enum;
using Newtonsoft.Json;

#endregion

namespace InsurgencyStatParser
{
    [Serializable]
    public class Config
    {
        public Setting MySetting;
        public Config()
        {
            MySetting = new Setting();
            var path = Directory.GetCurrentDirectory() + "\\config.json";

            if (File.Exists(path))
            {
               
                Load(path);
            }
            else
            { 
                SetOptions();
                Save(MySetting,path);
            }
        }

   

        private void SetOptions()
        {
            MySetting.AddConfig("TestServer",new FtpClientConfiguration{
                Host = "64.94.95.98",
                Port = 21,
                Username = "dglbot",
                Password = "dglbotpass",
                BaseDirectory = "/64.94.95.98_27025/insurgency/logs/",
                EncryptionType = FtpEncryption.None,
                SslProtocols = SslProtocols.None,
                IgnoreCertificateErrors = true
            });
            MySetting.DglApi = "https://httpbin.org/post";
            MySetting.TimeBetweenGrabs = 2;
            MySetting.MaxRoundsPerGame = 11;
            MySetting.MaxPerSwitch = 10;
            MySetting.RemoveFilesOnEnd = false;
        }

        private void Load(string location)
        {
            var json = File.ReadAllText(location);
            var settings = JsonConvert.DeserializeObject<Setting>(json);
            MySetting = settings;
            if (MySetting == null)
            {
                SetOptions();
                Save(MySetting, location);
                Load(location);
            }
        }

        public void Save()
        {
            var path = Directory.GetCurrentDirectory() + "\\config.json";
            Save(MySetting, path);
        }
        private void Save(Setting set, string path)
        {
            var json = JsonConvert.SerializeObject(set);
            if (File.Exists(path))
            {
                //deleate file   
                File.Delete(path);
            }
            //save
            File.AppendAllText(path, json);
        }
    }
    [Serializable]
    public class Setting
    {
        #region "Config Variable Definitions"

        public string DglApi { get; set; }
        public Dictionary<string,FtpClientConfiguration> FtpConfigs = new Dictionary<string, FtpClientConfiguration>(); 
        public int TimeBetweenGrabs { get; set; } 
        public int MaxRoundsPerGame { get; set; }
        public int MaxPerSwitch { get; set; }
        public bool RemoveFilesOnEnd { get; set; }
        #endregion

        public void AddConfig(string serverLocation,FtpClientConfiguration c)
        {
            FtpConfigs.Add(serverLocation,c);
        }

        public FtpClientConfiguration[] GetServers()
        {
            var serverList = new List<FtpClientConfiguration>();
            foreach (var s in FtpConfigs)
            {
                serverList.Add(s.Value);
            }

            return serverList.ToArray();
        }
        
        
    }
}