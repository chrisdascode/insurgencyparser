﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using CoreFtp;
using InsurgencyStatParser.Ftp;
using InsurgencyStatParser.GameLib;
using InsurgencyStatParser.Networking;
using InsurgencyStatParser.Networking.Ftp;
using Newtonsoft.Json;

namespace InsurgencyStatParser
{
    public static class Loader
    {
        private static GameManager _gameManager;
        private static Config _config;

        public static void Main(string[] args)
        {
            Console.Title = "TheDglPug LogParser by Coresails";
            _config = new Config();
            _gameManager = new GameManager(_config);
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromMinutes(_config.MySetting.TimeBetweenGrabs); //Added This as a config option
         
                Console.WriteLine("Enter an Option, [ addserver , start ]");
                var input = Console.ReadLine();
                if (input == null) return;
                if (input.Contains("addserver"))
                {                                                  // 0 1    2          3        4   5
                    Console.WriteLine("Enter a server like this: IP|PORT|DIRECTORY|USERNAME|PASS|ServerName");
                    var data = Console.ReadLine();
                    if (data == null) return;
                    var arg = data.Split("|");
                    var ip = arg[0];
                    var port = int.Parse(arg[1]);
                    var directory = arg[2];
                    var username = arg[3];
                    var password = arg[4];
                    var serverName = arg[5];
                    var config = new FtpClientConfiguration()
                    {
                        Host = ip,
                        Port = port,
                        Username = username,
                        Password = password,
                        BaseDirectory = directory,
                        IgnoreCertificateErrors = true
                    };
                    _config.MySetting.AddConfig(serverName,config);
                    _config.Save();
                }
             
            if (input.Contains("start"))
            {
                StartTimer(startTimeSpan, periodTimeSpan, _config);
            }
            
        }

        private static void StartTimer(TimeSpan startTimeSpan, TimeSpan periodTimeSpan, Config c)
        {
            using (new Timer(e =>
            {
                Servers();
                
            }, null, startTimeSpan, periodTimeSpan))
            {
                Console.WriteLine("...");
                Console.ReadLine();

            }
        }

        private static void Servers()
        {
            foreach (var server in _config.MySetting.FtpConfigs)
            {
                FtpHandler(server.Key,server.Value);
            }
        }
        private static void FtpHandler(string serverName,FtpClientConfiguration c)
        {
            using (var ftp = new ConnectNGrab(c,_config))
            {
                ftp.Connected += x => { 
                    Console.WriteLine($"Logged into {serverName}!");
                    
                };
                ftp.Disconnected += x =>
                {
                    Console.WriteLine($"Disconnected");
                    StartParser((List<Log>) x);
                };
                ftp.FTPError += x => { Console.WriteLine($"Error: {x}"); };
                ftp.Run();
            }
        }

        private static void StartParser(IEnumerable<Log> list)
        {
            try
            {
                using (var parser = new LogParser(list, _gameManager))
                {
                    parser.LogParsed += x =>
                    {
                        var gManager = (GameManager) x;
                        var properScore = gManager.PrepData();

                   
                        var json = JsonConvert.SerializeObject(properScore);

                        var wRequest = WebRequest.Create(_config.MySetting.DglApi);
                        Post.Send(wRequest,json);
                        var response = Post.GetResponce(wRequest).Result;
                        Console.WriteLine($"Response: {response}");
                    };
                    parser.Parse();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }
    }
}