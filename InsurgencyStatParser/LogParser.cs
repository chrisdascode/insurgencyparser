﻿#region "Imports"

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using InsurgencyStatParser.Ftp;
using InsurgencyStatParser.GameLib;

#endregion

namespace InsurgencyStatParser
{
    public class LogParser : IDisposable
    {
        private readonly GameManager _gManager;
        private readonly List<Log> _logList = new List<Log>();

        #region "regex consts"

        private const string ObjectiveCaptured = "\" triggered \"obj_captured\" (name \"\")";
        private const string TeamSecurityWin = "Team \"#Team_Security\" triggered \"Round_Win\"";
        private const string TeamInsurgentsWin = "Team \"#Team_Insurgent\" triggered \"Round_Win\"";
        private const string UserJoined = "<#Team_Unassigned>\" joined team \"";
        private const string PlayerKilled = "\" killed \"";
        private const string Restart = ": command \"mp_restartgame 1\"";
        private const string Ins = "Insurgent";
        private const string Sec = "Security";
        private const string Suicide = " committed suicide with ";

        #endregion
        
        public LogParser(IEnumerable<Log> list, GameManager manager)
        {
            _logList.AddRange(list);
            _gManager = manager;
        }

        public void Dispose()
        {
            //Console.WriteLine("Parser Disposed");
        }

        public void Parse()
        {
            foreach (var log in _logList)
            {
                if (!KeepLog(log)) continue;
                _gManager.StartGame();
                using (var stringReader = new StringReader(log.Data))
                {
                    string line;
                    do
                    {
                        line = stringReader.ReadLine();

                        if (line == null) continue;
                        if (line.Contains("Log file closed")) break;
                        if (line.Contains(UserJoined))
                        {
                            var rx = new Regex(UserJoined);
                            var array = rx.Split(line);
                            var team = array[1];
                            var steamId = array[0].Replace('>', ' ').Split('<')[2].Trim();
                            var myTeam = TeamSide.Error;
                            if (team.Contains(Sec)) myTeam = TeamSide.Security;
                            if (team.Contains(Ins)) myTeam = TeamSide.Insurgents;
                            HandleUserJoing(myTeam, steamId);
                            continue;
                        }

                        if (line.Contains(PlayerKilled))
                        {
                            var rx = new Regex(PlayerKilled);
                            var array = rx.Split(line);
                            var attacker = array[0].Replace('>', ' ').Split('<')[2].Trim();
                            var victim = array[1].Replace('>', ' ').Split('<')[2].Trim();
                            Playerkilled(attacker, victim);
                            continue;
                        }

                        if (line.Contains(Suicide))
                        {
                            var rx = new Regex(Suicide);
                            var array = rx.Split(line);
                            var steamId = array[0].Replace('>', ' ').Split('<')[2].Trim();
                            _gManager.Suicide(steamId);
                            continue;
                        }

                        if (line.Contains(TeamInsurgentsWin))
                        {
                            HandleInsurgentWin();
                            continue;
                        }

                        if (line.Contains(TeamSecurityWin))
                        {
                            HandleSecurityWin();
                            continue;
                        }

                        if (line.Contains(ObjectiveCaptured))
                        {
                            var rx = new Regex(ObjectiveCaptured);
                            var array = rx.Split(line);
                            var steamId = array[0].Replace('>', ' ').Split('<')[2].Trim();
                            HandleObjectiveCaptured(steamId);
                        }

                        if (line.Contains(Restart)) _gManager.ResetGame();
                        //TODO: Add Mp_switchTeams support
                    } while (line != null);
                }

                _gManager.EndGame();
            }

            LogParsed?.Invoke(_gManager);
        }

        private static bool KeepLog(Log checkLog)
        {
            return checkLog.Name.EndsWith("_002.log") && checkLog.Size > 464;
        }

        #region "Events"

        public delegate void EventHandler(object arg);

        public event EventHandler LogParsed;

        #endregion

        #region "Actions"

        private void HandleObjectiveCaptured(string steamId)
        {
            _gManager.ObjectiveCaptured(steamId);
        }

        private void HandleSecurityWin()
        {
            _gManager.HandleWin(TeamSide.Security);
        }

        private void HandleInsurgentWin()
        {
            _gManager.HandleWin(TeamSide.Insurgents);
        }

        private void Playerkilled(string attackerSteamId, string victimSteamId)
        {
            _gManager.PlayerKilled(attackerSteamId, victimSteamId);
        }

        private void HandleUserJoing(TeamSide team, string steamId)
        {
            _gManager.UserJoined(team, steamId);
        }

        #endregion
    }
}