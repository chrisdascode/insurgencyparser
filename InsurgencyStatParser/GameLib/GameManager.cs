﻿using System;
using System.Collections.Generic;

namespace InsurgencyStatParser.GameLib
{
    [Serializable]
    public class GameScore
    {
        public readonly Score Gamescore = new Score();
        public readonly List<Player> Insurgents = new List<Player>();
        public readonly List<Player> Security = new List<Player>();
        
    }

    public class Score
    {
        public int Sec { get; set; }
        public int Ins { get; set; }
    }

    public class GameManager
    {
        private Score _score;
        private int _currentRound;
        public GameManager(Config c)
        {
            CurrentGame = new Game();
            _score = new Score();
            MaxPerSwitch = c.MySetting.MaxPerSwitch;
            MaxRoundsPerGame = c.MySetting.MaxRoundsPerGame;
            _currentRound = 0;
        }

        private Game CurrentGame { get; }

        private int MaxRoundsPerGame { get; }

        private int MaxPerSwitch { get; }

        public void UserJoined(TeamSide team, string steamId)
        {
            CurrentGame.CurrentRound.AddPlayer(team, steamId);
        }

        public void StartGame()
        {
            CurrentGame.StartGame();
        }

        public void EndGame()
        {
            CurrentGame.EndGame();
        }

        public void PlayerKilled(string attacker, string victim)
        {
            CurrentGame.PlayerKilled(attacker, victim);
        }

        public void ObjectiveCaptured(string steamId)
        {
            CurrentGame.ObjectiveCaptured(steamId);
        }

        public void HandleWin(TeamSide winner)
        {
            
            if (MaxPerSwitch == _currentRound)
            {
                CurrentGame.CurrentRound.SwitchTeams();
                var s1 = _score.Ins;
                var s2 = _score.Sec;
                _score.Ins = s2;
                _score.Sec = s1;
            }
            else
            {
                if (MaxRoundsPerGame == _currentRound) EndGame();
            }

            switch (winner)
            {
                case TeamSide.Insurgents:
                    _score.Ins++;
                    break;
                case TeamSide.Security:
                    _score.Sec++;
                    break;
                case TeamSide.Spectators:
                    break;
                case TeamSide.Error:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(winner), winner, null); //FtpError?
            }

            CurrentGame.EndRound();
            _currentRound++;
        }

        private void CpyPlayers()
        {
            var teams = CurrentGame.CurrentRound.GetTeams();
            var dict = new Dictionary<Player, TeamSide>();
            foreach (var team in teams)
            foreach (var player in team.Players)
                dict.Add(player, team.Name);
            CurrentGame.ResetGame();
            _score = new Score();
            foreach (var line in dict) UserJoined(line.Value, line.Key.SteamId);
        }

        public void ResetGame()
        {
            CpyPlayers();
        }

        private Score GetScore()
        {
            return _score;
        }

        private IEnumerable<Team> GetTeams()
        {
            var cRound = CurrentGame.CurrentRound;
            var ins = cRound.Insurgents;
            var sec = cRound.Security;
            var spec = cRound.Spectators;
            return new[] {ins, sec, spec};
        }

        public void Suicide(string steamId)
        {
            CurrentGame.Suicide(steamId);
        }

        public GameScore PrepData()
        {
            var score = new GameScore();
            var s = GetScore();
            foreach (var team in GetTeams())
            foreach (var player in team.Players)
            {
                var id = player.SteamId;
                var k = player.Kills;
                var d = player.Deaths;
                var o = player.Objectives;
                switch (team.Name)
                {
                    case TeamSide.Insurgents:
                        score.Insurgents.Add(new Player
                        {
                            SteamId = id,
                            Kills = k,
                            Deaths = d,
                            Objectives = o
                        });
                        break;
                    case TeamSide.Security:
                        score.Security.Add(new Player
                        {
                            SteamId = id,
                            Kills = k,
                            Deaths = d,
                            Objectives = o
                        });
                        break;
                    case TeamSide.Spectators:
                        break;
                    case TeamSide.Error:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            score.Gamescore.Ins = s.Ins;
            score.Gamescore.Sec = s.Sec;
            return score;
        }
    }
}