﻿using System;
using System.Collections.Generic;

namespace InsurgencyStatParser.GameLib
{
    [Serializable] //Needed for Json Serialziation
    public class Team
    {
        public List<Player> Players = new List<Player>();

        public Team(TeamSide teamName)
        {
            Name = teamName;
        }

        public TeamSide Name { get; }

        public void AddPlayer(string steamId)
        {
            Players.Add(new Player {SteamId = steamId});
        }

        public void AddPlayer(Player player)
        {
            Players.Add(player);
        }

        public void AddKill(string steamId, int amt = 0)
        {
            foreach (var player in Players)
                if (player.SteamId.Equals(steamId))
                    if (amt == 0)
                        player.Kills++;
                    else
                        player.Kills = amt;
        }

        public void AddDeath(string steamId, int amt = 0)
        {
            foreach (var player in Players)
                if (player.SteamId.Equals(steamId))
                    if (amt == 0)
                        player.Deaths++;
                    else
                        player.Deaths = amt;
        }

        public void AddObjective(string steamId, int amt = 0)
        {
            foreach (var player in Players)
                if (player.SteamId.Equals(steamId))
                    if (amt == 0)
                        player.Objectives++;
                    else
                        player.Objectives = amt;
        }

        public void RemovePlayer(string steamId)
        {
            foreach (var player in Players)
                if (player.SteamId.Equals(steamId))
                    Players.Remove(player);
        }
    }
}