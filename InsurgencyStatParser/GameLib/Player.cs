﻿using System;

namespace InsurgencyStatParser.GameLib
{
    [Serializable] //Needed for Json Serialziation
    public class Player
    {
        public string SteamId { get; set; }
        public int Kills { get; set; } // Variable for kills for each player
        public int Deaths { get; set; } // Variable for Deaths for each player
        public int Objectives { get; set; } // Variable for Objectives for each player
    }
}