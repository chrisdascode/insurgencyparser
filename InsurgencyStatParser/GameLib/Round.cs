﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InsurgencyStatParser.GameLib
{
    [Serializable]
    public class Round
    {
        private readonly Team[] Teams;

        //Start
        public Round()
        {
            Insurgents = new Team(TeamSide.Insurgents);
            Security = new Team(TeamSide.Security);
            Spectators = new Team(TeamSide.Spectators);
            Teams = new[] {Insurgents, Security, Spectators};
        }

        public Team Insurgents { get; set; }
        public Team Security { get; set; }
        public Team Spectators { get; }

        public void AddPlayer(TeamSide team, string steamId)
        {
            var player = FindPlayerById(steamId);
            if (player != null) return; // Player already exists!
            if (team == TeamSide.Spectators) Spectators.AddPlayer(steamId);
            if (team == TeamSide.Insurgents) Insurgents.AddPlayer(steamId);
            if (team == TeamSide.Security) Security.AddPlayer(steamId);
        }

        public void AddPlayer(TeamSide team, Player player)
        {
            // var player = FindPlayerById(steamId);
            if (team == TeamSide.Spectators) Spectators.AddPlayer(player);
            if (team == TeamSide.Insurgents) Insurgents.AddPlayer(player);
            if (team == TeamSide.Security) Security.AddPlayer(player);
        }

        public void RemovePlayer(TeamSide team, string steamId)
        {
            var player = FindPlayerById(steamId);
            if (player == null)
            {
                if (team == TeamSide.Spectators) Spectators.RemovePlayer(steamId);
                if (team == TeamSide.Insurgents) Insurgents.RemovePlayer(steamId);
                if (team == TeamSide.Security) Security.RemovePlayer(steamId);
            } // Player Doesnt exists!
        }

        public IEnumerable<Team> GetTeams()
        {
            return new[] {Insurgents, Security, Spectators};
        }

        public void PlayerKilled(string attacker, string victim)
        {
            var a = FindPlayerById(attacker);
            var v = FindPlayerById(victim);
            if (a == null) return;
            if (v == null) return;
            a.Kills++;
            v.Deaths++;
        }

        public void Suicide(string steamId)
        {
            var a = FindPlayerById(steamId);
            if (a == null) return;
            a.Deaths++;
        }

        public void ObjectiveCaptured(string steamId)
        {
            var player = FindPlayerById(steamId);
            if (player == null) return;
            player.Objectives++;
        }

        public Player FindPlayerById(string steamId)
        {
            return Teams.SelectMany(team => team.Players).FirstOrDefault(p => p.SteamId.Equals(steamId));
        }

        public TeamSide FindTeamByPlayerId(string steamId)
        {
            return (from team in Teams
                from player in team.Players
                where player.SteamId.Equals(steamId)
                select team.Name).FirstOrDefault();
        }

        public Team GetTeamBySide(TeamSide side)
        {
            foreach (var team in Teams)
            {
                if (team.Name == TeamSide.Insurgents) return Insurgents;

                if (team.Name == TeamSide.Security) return Security;

                if (team.Name == TeamSide.Spectators) return Spectators;
            }

            return new Team(TeamSide.Error);
        }

        public void SwitchTeams()
        {
            var teams2Switch = new[] {Insurgents, Security};
            var ins2sec = new Team(TeamSide.Security);
            var sec2ins = new Team(TeamSide.Insurgents);
            foreach (var team in teams2Switch)
            foreach (var p in team.Players)
            {
                if (team.Name.Equals(TeamSide.Security))
                {
                    var steamid = p.SteamId;
                    var kills = p.Kills;
                    var deaths = p.Deaths;
                    var obj = p.Objectives;
                    sec2ins.AddPlayer(steamid);
                    if (kills > 0) sec2ins.AddKill(steamid, p.Kills);
                    if (deaths > 0) sec2ins.AddDeath(steamid, p.Deaths);
                    if (obj > 0) sec2ins.AddObjective(steamid, p.Objectives);
                }

                if (team.Name.Equals(TeamSide.Insurgents))
                {
                    var steamid = p.SteamId;
                    var kills = p.Kills;
                    var deaths = p.Deaths;
                    var obj = p.Objectives;
                    ins2sec.AddPlayer(steamid);
                    if (kills > 0) ins2sec.AddKill(steamid, p.Kills);
                    if (deaths > 0) ins2sec.AddDeath(steamid, p.Deaths);
                    if (obj > 0) ins2sec.AddObjective(steamid, p.Objectives);
                }
            }

            Insurgents = sec2ins;
            Security = ins2sec;
        }
    }
}