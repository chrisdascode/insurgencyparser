﻿using System;
using System.Collections.Generic;

namespace InsurgencyStatParser.GameLib
{
    [Serializable] //Needed for Json Serialziation
    public class Game
    {
        public List<Round> Rounds = new List<Round>();


        public Round CurrentRound { get; private set; }

        public void StartGame()
        {
            CurrentRound = new Round();
        }

        public void EndGame()
        {
            Rounds.Add(CurrentRound);
        }

        public void PlayerKilled(string attacker, string victim)
        {
            CurrentRound.PlayerKilled(attacker, victim);
        }

        public void ObjectiveCaptured(string steamId)
        {
            CurrentRound.ObjectiveCaptured(steamId);
        }

        public void ResetGame()
        {
            Rounds.Clear();
            StartGame();
        }

        public Dictionary<Player, TeamSide> GrabPlayers()
        {
            var list = new Dictionary<Player, TeamSide>();
            foreach (var team in CurrentRound.GetTeams())
            foreach (var player in team.Players)
                list.Add(player, team.Name);

            return list;
        }

        public void Suicide(string steamId)
        {
            CurrentRound.Suicide(steamId);
        }

        public void EndRound()
        {
            var listOPlayers = GrabPlayers();
            Rounds.Add(CurrentRound);
            CurrentRound = new Round();
            foreach (var item in listOPlayers) CurrentRound.AddPlayer(item.Value, item.Key);
        }
    }
}