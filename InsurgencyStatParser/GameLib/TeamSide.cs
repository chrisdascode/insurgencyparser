﻿using System;

namespace InsurgencyStatParser.GameLib
{
    [Serializable]
    public enum TeamSide
    {
        Security = 1,
        Insurgents = 2,
        Spectators = 3,
        Error = 0
    }
}